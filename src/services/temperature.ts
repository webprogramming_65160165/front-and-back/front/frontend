import http from "./http"

type ReturnData = {
    celsius: number,
    fahrenheit: number
}
async function convert(celsius: number): Promise<number> {
    console.log('service: call convert')
    console.log(`/temperature/convert/${celsius}`)
    const res = await http.post(`/temperature/convert`, { celsius: celsius })
    const convertResult = res.data as ReturnData
    console.log('service: finish call convert')

    return convertResult.fahrenheit
}

export default { convert }